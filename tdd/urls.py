from django.urls import re_path
from .views import index, add_status

urlpatterns = [
	re_path(r'^$', index, name='index'),
	re_path(r'^add_status', add_status, name='add_status'),
]
