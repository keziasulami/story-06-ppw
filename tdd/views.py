from django.shortcuts import render, redirect
from django.urls import reverse
from .models import Status
from .forms import StatusForm

# Create your views here.
def index(request):
	status_form = StatusForm()
	context = {
		'status_form' : status_form,
		'statuses' : Status.objects.all()
	}
	return render(request, 'index.html', context)

def add_status(request):
	status = StatusForm(request.POST)
	status.save()
	return redirect(reverse('index'))
