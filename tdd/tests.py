from django.test import TestCase, Client
from .models import Status
import unittest

# Create your tests here.
class UnitTest(TestCase):

	def test_apakah_ada_url_slash(self):
		c = Client()
		response = c.get('/')
		self.assertEqual(response.status_code, 200)

	def test_apakah_ada_text_box(self):		
		c = Client()
		response = c.get('/')
		content = response.content.decode('utf8')
		self.assertIn('<input type="text"', content)

	def test_apakah_ada_input_submit(self):
		c = Client()
		response = c.get('/')
		content = response.content.decode('utf8')
		self.assertIn("<input", content)
		self.assertIn("Submit", content)

	def test_apakah_ada_table(self):
		c = Client()
		response = c.get('/')
		content = response.content.decode('utf8')
		self.assertIn("<table", content)

	def test_apakah_kalo_text_box_diisi_inputnya_masuk(self):
		c = Client()
		yang_mau_ditest = "lorem ipsum"
		c.post('/add_status', data={'status': yang_mau_ditest})
		response = c.get('/')
		content = response.content.decode('utf8')
		self.assertIn(yang_mau_ditest, content)

	def test_apakah_ada_model_status_dengan_field_status_dan_datetime(self):
		Status(status="coba").save()
		semua_status = Status.objects.all()
		self.assertEqual(semua_status.count(), 1)
		self.assertEqual(Status.objects.get(id=1).status, "coba")

from selenium import webdriver
from selenium.webdriver.chrome.options import Options

class FunctionalTest(unittest.TestCase):

	url = 'http://localhost:8000'

	def setUp(self):
		options = Options()
		options.add_argument('--headless')
		options.add_argument('--no-sandbox')
		options.add_argument('--disable-dev-shm-usage')
		self.browser = webdriver.Chrome(chrome_options=options)
		self.browser.get(self.url)

	def tearDown(self):
		self.browser.quit()

	def test_judul(self):
		browser = self.browser
		self.assertIn("Status", browser.title)

	def test_status(self):
		browser = self.browser
		str_input = "testing"
		text_area = browser.find_element_by_id("id_status")
		text_area.send_keys(str_input)
		sub_button = browser.find_element_by_id("submit")
		sub_button.click()
		self.assertIn(str_input, browser.page_source)
